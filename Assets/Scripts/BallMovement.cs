﻿using UnityEngine;
using System.Collections;

public class BallMovement : MonoBehaviour 
{
    private GameObject table;
    private float lengthOfTable;
    private float maxDivertAngle;
    public float ballSpeed;
    public Vector3 ballSpeedVec;
    private Vector3 intersectVector;
    private float intersectPoint;
    private static bool isRendered = false;

    public float initialBallSpeed;

	void Awake () 
    {
        table = GameObject.Find("Table");
        lengthOfTable = table.transform.localScale.z;
        ballSpeed = initialBallSpeed;
        ballSpeedVec = new Vector3(0,0,1);
        maxDivertAngle = 75;
        resetBall();
	}
	
	void FixedUpdate () 
    {
        //if(isRendered)
        //{
            this.gameObject.transform.Translate(ballSpeedVec * ballSpeed/100);

            Collider[] hit = Physics.OverlapSphere(transform.position, 0.1f);
            for (int i = 0; i < hit.Length; i++)
            {
                if (hit[i].gameObject.tag == "ScoreWall")
                {
                    //add points //reset the ball to center //add random rotation
                    resetBall();
                }
                else if (hit[i].gameObject.tag == "SideWall")
                {
                    //Vector3 incidentVector = this.gameObject.transform.position - intersectVector;
                    //float angle = Vector3.Angle(incidentVector,Vector3.right);                  

                    //change the y rotation sign
                    transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, -transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                }
                else if (hit[i].gameObject.tag == "Paddle")
                {
                    intersectVector = (this.gameObject.transform.position - hit[i].gameObject.transform.position);
                    //Debug.Log("Paddle pos" + hit[i].gameObject.transform.position + "Ball pos" + this.gameObject.transform.position + "Intersect Vector" + intersectVector);
                    //Debug.Log("intersectVector * 10" + intersectVector * 10);
                    
                    Vector3 normalizedRelIntersection = intersectVector.normalized;
                    intersectPoint = intersectVector.x;
                    Debug.Log("normalizedRelIntersection" + normalizedRelIntersection);

                    float bounceAngle = intersectPoint * maxDivertAngle;

                    //ballSpeedVec.x = (ballSpeed / 2) * Mathf.Cos(bounceAngle);

                    //ballSpeedVec.z = (ballSpeed / 2) * -Mathf.Sin(bounceAngle);
                    transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, -transform.rotation.eulerAngles.y + 180, transform.rotation.eulerAngles.z);
                }
            }
        //}
	}

    public static void checkRendering(bool check)
    {
        isRendered = check;
    }

    void resetBall()
    {
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, Random.Range(0, 359), transform.rotation.eulerAngles.z);
        this.transform.localPosition = new Vector3(0, 0.07f, 0);
        ballSpeedVec = new Vector3(0, 0, 1);
    }

    void OnGUI()
    {
        if (GUI.Button(new Rect(50, 50, 220, 100), "Reset Ball"))
        {
            resetBall();
        }
    }
}
