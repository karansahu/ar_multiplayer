﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PowerUp : MonoBehaviour 
{
	public GameObject Paddle1;
	public GameObject Paddle2;
	//public GameObject[] ball;
	public List<GameObject> ballList = new List<GameObject>();
	public static bool checkRender;
	public GameObject tempball;
	private Transform spawnPos;
	// Use this for initialization

	public void checkRendering () 
	{	
		Paddle1 = GameObject.Find ("paddle1");
		Paddle2 = GameObject.Find ("paddle2");
		GameObject temp = GameObject.Find ("ballPrefab");
		ballList.Add (temp);
	}
	
	// Update is called once per frame
	void Update () {
		if (checkRender == true && ballList.Count <1)
			checkRendering ();
	}

	void SizeUp()
	{
		foreach(GameObject i in ballList)
		{
			i.transform.localScale = new Vector3(0.5f,0.5f,0.5f);
		}
	}

	void SpeedUp()
	{
		foreach (GameObject i in ballList) {
			i.GetComponent<BallMovement>().ballSpeed +=1f;
		}

	}
	void SpeedDown()
	{
		foreach (GameObject i in ballList) {
			i.GetComponent<BallMovement>().ballSpeed -=1f;
		}
		
	}
	void SizeDown()
	{
		foreach(GameObject i in ballList)
		{
			i.transform.localScale  = new Vector3(.5f,.5f,.5f);
		}

	}
	void SpawnBall()
	{	tempball = ballList [ballList.Count - 1];
		GameObject newball = PhotonNetwork.Instantiate("ballPrefab",tempball.transform.position,Quaternion.identity,0);
		ballList.Add (newball);
	}




}
